import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import couriers from './modules/couriers.module'
import orders from './modules/orders.module'

const modules = {
  orders,
  couriers,
}

export default new Vuex.Store({
  modules,
})
