import { apiEndpoint } from '../../shared/index';
import ApiService from '../../services/api.services';

const apiOrders = `${apiEndpoint}orders`;
const apiOrder = `${apiEndpoint}order`;

const state = {
    data: null,
    pending: false,
    error: null,
};

const getters = {
    loaded: (state) => !state.pending && state.data != null,
    infoOrders: (state, getters) => (getters.loaded ? state.data : []),
};

const mutations = {
    data(state, { data = null, error = null } = {}) {
        state.pending = false;
        state.data = JSON.parse(JSON.stringify(data));
        state.error = error;
    },

    deleted(state, id, error) {
        state.pending = false;

        id.ids.forEach(t => {
            const idx = state.data.orders.findIndex((order) => order.id === t);
            if (idx !== -1) {
                state.data.orders.splice(idx, 1);
            }
        })


        state.error = error;
    },

};


const actions = {
    async fetchDataOrders({commit}) {
        try {
            const { data } = await ApiService.get(`${apiOrders}`);
            commit('data', { data });
        } catch (error) {
            console.error(error)
            commit('data', { error });
        }
    },
    async deleteOrder({ commit }, id) {
        try {
            await ApiService.post(`${apiOrder}`, id);
            commit('deleted', id);
        } catch (error) {
            console.error(error)
            commit('deleted', error);
        }
    },


};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};
