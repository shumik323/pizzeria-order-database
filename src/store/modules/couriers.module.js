import { apiEndpoint } from '../../shared/index';
import ApiService from '../../services/api.services';

const apiCouriers = `${apiEndpoint}/couriers`;


const state = {
    data: null,
    pending: false,
    error: null,
};

const getters = {
    loaded: (state) => !state.pending && state.data != null,
    infoCouriers: (state, getters) => (getters.loaded ? state.data : []),
};

const mutations = {
    data(state, { data = null, error = null } = {}) {
        state.pending = false;
        state.data = data;
        state.error = error;
    },

};


const actions = {
    async fetchDataCouriers({commit}) {
        try {
            const { data } = await ApiService.get(`${apiCouriers}`);
            commit('data', { data });

        } catch (error) {
            console.error(error)
            commit('data', { error });
        }
    },


};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};
