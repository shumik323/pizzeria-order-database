import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '../src/components/component-list'
const moment = require('moment')
require('moment/locale/ru')
Vue.use(require('vue-moment'), {
  moment
})
Vue.moment().locale()

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
