import Vue from "vue";
import TheNavbar from "./app/TheNavbar";
import AppTableBody from "./table/AppTableBody";
import AppTableHead from "./table/AppTableHead";
import AppTableTemplate from "./table/AppTableTemplate";
import AppButton from "./app/AppButton/AppButton";
import AppStatus from "./app/AppStatus";
import TheActionFooter from "./app/TheActionFooter/TheActionFooter";
import TheNavigate from "./app/TheNavigate";
import TheSearch from "./app/TheSearch/TheSearch";
import AppPointDropdown from "./app/AppPointDropdown/AppPointDropdown";
import TheFilters from "./app/TheFilters/TheFilters";

Vue.component(TheNavbar.name, TheNavbar)
Vue.component(AppTableBody.name, AppTableBody)
Vue.component(AppTableHead.name, AppTableHead)
Vue.component(AppTableTemplate.name, AppTableTemplate)
Vue.component(AppButton.name, AppButton)
Vue.component(AppStatus.name, AppStatus)
Vue.component(TheActionFooter.name, TheActionFooter)
Vue.component(TheNavigate.name, TheNavigate)
Vue.component(TheSearch.name, TheSearch)
Vue.component(AppPointDropdown.name, AppPointDropdown)
Vue.component(TheFilters.name, TheFilters)


