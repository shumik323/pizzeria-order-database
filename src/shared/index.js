export const apiEndpoint = process.env.VUE_APP_API_ENDPOINT_URL;

export default {
    apiEndpoint,
};
